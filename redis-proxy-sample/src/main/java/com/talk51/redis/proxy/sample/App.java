/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.talk51.redis.proxy.sample;

import com.redis.proxy.client.factory.ProxyRdsConnFactory;
import com.redis.proxy.client.keys.DelayRdsBuild;
import com.redis.proxy.client.templates.ProxyRdsTemplate;
import com.talk51.redis.proxy.sample.vo.TestVo;
import java.util.concurrent.TimeUnit;
import redis.clients.jedis.Jedis;

/**
 * 测试项目-测试用
 *
 * @author zhanggaofeng
 */
public class App {

        private final static DelayRdsBuild testKey = new DelayRdsBuild("module1", "test", 1, 1, TimeUnit.DAYS, TestVo.class);

        public static void main(String... args) {
                test1();
                test2();
        }

        /**
         * 使用自带模板方式
         */
        private static void test1() {
                ProxyRdsConnFactory factory = new ProxyRdsConnFactory("127.0.0.1:2181,127.0.0.1:2181,127.0.0.1:2181", 5000, 5000, 2, 10);
                ProxyRdsTemplate redisTemplate = new ProxyRdsTemplate(factory);
                try {
                        TestVo test = new TestVo("zhanggaofeng", (int) (Math.random() * 1000), "河南");
                        redisTemplate.set(testKey.build("1"), test);
                        TestVo value = redisTemplate.get(testKey.build("1"));
                        System.err.println(value);
                } finally {
                        redisTemplate.destory();
                }
        }

        /**
         * 直接使用jedis方法
         */
        private static void test2() {
                String redisKey = "module1_test1_1_1";
                ProxyRdsConnFactory factory = new ProxyRdsConnFactory("127.0.0.1:2181,127.0.0.1:2181,127.0.0.1:2181", 5000, 5000, 2, 10);
                Jedis jedis = factory.getPool(redisKey);
                jedis.set(redisKey, "100");
                System.err.println(jedis.get(redisKey));
        }
}
